---
title: About
---
How to build a machine learning model that is fully aware of the laws of physics?

Such a model may solve problems that are extremely difficult or computationally intensive using standard discretisation methods.

Welcome to Machine Learning with Physics, a website dedicated to the integration of physics into machine learning models.

Feel free to [contact me](/contact/).